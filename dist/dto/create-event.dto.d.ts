export declare class CreateEventDto {
    eventname: string;
    description: string;
    initialDatetime: Date;
    finalDatetime: Date;
}
