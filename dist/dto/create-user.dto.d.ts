export declare class CreateUserDto {
    username: string;
    email: string;
    password: string;
    gender: string;
    cell: string;
}
