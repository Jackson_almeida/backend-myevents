"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventController = void 0;
const create_event_dto_1 = require("../dto/create-event.dto");
const common_1 = require("@nestjs/common");
const event_service_1 = require("./event.service");
const user_decorator_1 = require("../decorators/user.decorator");
let EventController = class EventController {
    constructor(eventService) {
        this.eventService = eventService;
    }
    async findAll(user) {
        console.log(user);
        return this.eventService.getAllByUser(user.userId);
    }
    async getById(id) {
        return this.eventService.getById(id);
    }
    async createEvent(user, createEventDto) {
        console.log(user);
        return this.eventService.createEvent(user.userId, createEventDto);
    }
    async updateEvent(id, createEventDto) {
        return {
            message: 'Evento atualizado com sucesso!',
            data: this.eventService.updateEvent(+id, createEventDto),
        };
    }
    async deleteEventId(id) {
        return this.eventService.deleteEventId(id);
    }
};
__decorate([
    common_1.Get(),
    __param(0, user_decorator_1.JwtUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "getById", null);
__decorate([
    common_1.Post(),
    __param(0, user_decorator_1.JwtUser()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, create_event_dto_1.CreateEventDto]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "createEvent", null);
__decorate([
    common_1.Put(':id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, create_event_dto_1.CreateEventDto]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "updateEvent", null);
__decorate([
    common_1.Delete(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "deleteEventId", null);
EventController = __decorate([
    common_1.Controller('events'),
    __metadata("design:paramtypes", [event_service_1.EventService])
], EventController);
exports.EventController = EventController;
//# sourceMappingURL=event.controller.js.map