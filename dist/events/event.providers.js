"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.eventProviders = void 0;
const event_entity_1 = require("../entities/event.entity");
exports.eventProviders = [
    {
        provide: 'EVENT_REPOSITORY',
        useFactory: (connection) => connection.getRepository(event_entity_1.Event),
        inject: ['DATABASE_CONNECTION'],
    },
];
//# sourceMappingURL=event.providers.js.map