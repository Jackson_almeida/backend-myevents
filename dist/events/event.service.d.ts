import { Repository } from 'typeorm';
import { Event } from '../entities/event.entity';
import { CreateEventDto } from '../dto/create-event.dto';
import { User } from 'src/entities/user.entity';
export declare class EventService {
    private eventRepository;
    private userRepository;
    constructor(eventRepository: Repository<Event>, userRepository: Repository<User>);
    getAll(): Promise<Event[]>;
    getAllByUser(id: number): Promise<Event[]>;
    getById(id: number): Promise<Event>;
    createEvent(userId: number, createEventDto: CreateEventDto): Promise<Event>;
    updateEvent(id: number, createEventDto: CreateEventDto): Promise<Event>;
    deleteEventId(id: number): Promise<any>;
    invite(): Promise<void>;
}
