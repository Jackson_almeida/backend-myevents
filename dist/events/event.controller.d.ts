import { CreateEventDto } from '../dto/create-event.dto';
import { Event } from '../entities/event.entity';
import { EventService } from './event.service';
export declare class EventController {
    private readonly eventService;
    constructor(eventService: EventService);
    findAll(user: any): Promise<Event[]>;
    getById(id: number): Promise<Event>;
    createEvent(user: any, createEventDto: CreateEventDto): Promise<Event>;
    updateEvent(id: number, createEventDto: CreateEventDto): Promise<{
        message: string;
        data: Promise<Event>;
    }>;
    deleteEventId(id: number): Promise<any>;
}
