"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const event_entity_1 = require("../entities/event.entity");
const user_entity_1 = require("../entities/user.entity");
const user_service_1 = require("../users/user.service");
let EventService = class EventService {
    constructor(eventRepository, userRepository) {
        this.eventRepository = eventRepository;
        this.userRepository = userRepository;
    }
    async getAll() {
        return this.eventRepository.find();
    }
    async getAllByUser(id) {
        return this.eventRepository.find({ where: { user: id } });
    }
    async getById(id) {
        return this.eventRepository.findOne(id);
    }
    async createEvent(userId, createEventDto) {
        const event = new event_entity_1.Event();
        Object.assign(event, createEventDto);
        const user = await this.userRepository.findOne(userId);
        event.user = user;
        return this.eventRepository.save(event);
    }
    async updateEvent(id, createEventDto) {
        const event = await this.eventRepository.findOne(id);
        event.eventname = createEventDto.eventname;
        event.description = createEventDto.description;
        event.initialDatetime = createEventDto.initialDatetime;
        event.finalDatetime = createEventDto.finalDatetime;
        return this.eventRepository.save(event);
    }
    async deleteEventId(id) {
        return await this.eventRepository.delete(id);
    }
    async invite() {
    }
};
EventService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(event_entity_1.Event)),
    __param(1, typeorm_1.InjectRepository(user_entity_1.User)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository])
], EventService);
exports.EventService = EventService;
//# sourceMappingURL=event.service.js.map