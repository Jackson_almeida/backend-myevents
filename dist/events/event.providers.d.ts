import { Connection, Repository } from 'typeorm';
import { Event } from '../entities/event.entity';
export declare const eventProviders: {
    provide: string;
    useFactory: (connection: Connection) => Repository<Event>;
    inject: string[];
}[];
