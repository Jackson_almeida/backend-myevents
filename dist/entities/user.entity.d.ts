import { Event } from './event.entity';
export declare class User {
    id: number;
    username: string;
    email: string;
    password: string;
    events: Event[];
}
