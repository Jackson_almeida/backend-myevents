import { User } from './user.entity';
export declare class Event {
    id: number;
    eventname: string;
    description: string;
    initialDatetime: Date;
    finalDatetime: Date;
    user: User;
    guests: User[];
}
