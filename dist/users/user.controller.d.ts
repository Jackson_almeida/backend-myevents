import { AuthService } from './../auth/auth.service';
import { CreateUserDto } from '../dto/create-user.dto';
import { User } from '../entities/user.entity';
import { UserService } from './user.service';
export declare class UserController {
    private readonly userService;
    private authService;
    constructor(userService: UserService, authService: AuthService);
    getById(id: number): Promise<User>;
    getByEmail(email: string): Promise<User>;
    createUser(createUserDto: CreateUserDto): Promise<User>;
    login(req: any): Promise<{
        access_token: string;
    }>;
}
