import { Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { CreateUserDto } from '../dto/create-user.dto';
export declare class UserService {
    private userRepository;
    constructor(userRepository: Repository<User>);
    getAll(): Promise<User[]>;
    getById(id: number): Promise<User>;
    getByEmail(email: string): Promise<User>;
    getByUserEmail(email: string): Promise<User | undefined>;
    createUser(createUserDto: CreateUserDto): Promise<User>;
    updateUser(id: number, createUserDto: CreateUserDto): Promise<User>;
    deleteUserId(id: number): Promise<string>;
}
