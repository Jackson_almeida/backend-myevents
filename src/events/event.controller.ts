import { CreateEventDto } from '../dto/create-event.dto';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Event } from '../entities/event.entity';
import { EventService } from './event.service';
import { JwtUser } from 'src/decorators/user.decorator';

@Controller('events')
export class EventController {
  constructor(private readonly eventService: EventService) {}

  @Get()
  async findAll(@JwtUser() user): Promise<Event[]> {
    console.log(user);
    return this.eventService.getAllByUser(user.userId);
  }

  @Get(':id')
  async getById(@Param('id') id: number): Promise<Event> {
    return this.eventService.getById(id);
  }

  @Post()
  async createEvent(@JwtUser() user, @Body() createEventDto: CreateEventDto) {
    console.log(user);
    return this.eventService.createEvent(user.userId, createEventDto);
  }

  @Put(':id')
  async updateEvent(
    @Param('id') id: number,
    @Body() createEventDto: CreateEventDto,
  ) {
    return {
      message: 'Evento atualizado com sucesso!',
      data: this.eventService.updateEvent(+id, createEventDto),
    };
  }

  @Delete(':id')
  async deleteEventId(@Param('id') id: number): Promise<any> {
    return this.eventService.deleteEventId(id);
  }
}
