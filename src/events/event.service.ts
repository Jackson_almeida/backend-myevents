import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Event } from '../entities/event.entity';
import { CreateEventDto } from '../dto/create-event.dto';
import { User } from 'src/entities/user.entity';
import { UserService } from 'src/users/user.service';

@Injectable()
export class EventService {
  constructor(
    @InjectRepository(Event)
    private eventRepository: Repository<Event>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async getAll(): Promise<Event[]> {
    return this.eventRepository.find();
  }

  async getAllByUser(id: number): Promise<Event[]> {
    return this.eventRepository.find({ where: { user: id } });
  }

  async getById(id: number): Promise<Event> {
    return this.eventRepository.findOne(id);
  }

  async createEvent(
    userId: number,
    createEventDto: CreateEventDto,
  ): Promise<Event> {
    const event = new Event();
    Object.assign(event, createEventDto);

    const user = await this.userRepository.findOne(userId);
    event.user = user;

    return this.eventRepository.save(event);
  }

  async updateEvent(
    id: number,
    createEventDto: CreateEventDto,
  ): Promise<Event> {
    const event = await this.eventRepository.findOne(id);
    event.eventname = createEventDto.eventname;
    event.description = createEventDto.description;
    event.initialDatetime = createEventDto.initialDatetime;
    event.finalDatetime = createEventDto.finalDatetime;
    return this.eventRepository.save(event);
  }

  async deleteEventId(id: number): Promise<any> {
    return await this.eventRepository.delete(id);
    // await this.eventRepository.delete(id);
    // return {
    //   msg: 'Evento deletado com sucesso!'
    // };
  }

  async invite() {
    // verificar se o usuário convidado existe
    // depois verificar se ele é dono do evento a qual ele está convidando
  }
}
