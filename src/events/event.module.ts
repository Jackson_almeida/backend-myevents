import { EventController } from './event.controller';
import { Module } from '@nestjs/common';
import { EventService } from './event.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Event } from 'src/entities/event.entity';
import { User } from 'src/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Event, User])],
  exports: [TypeOrmModule],
  controllers: [EventController],
  providers: [EventService],
})
export class EventModule {}
