// o arquivo de entrada do aplicativo que usa a função principal NestFactory para criar uma instância do aplicativo Nest.

import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.enableCors();
  await app.listen(5000);
}
bootstrap();
