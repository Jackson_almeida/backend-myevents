import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { User } from './user.entity';

@Entity()
export class Event {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100 })
  eventname: string;

  @Column({ length: 255 })
  description: string;

  @Column()
  initialDatetime: Date;

  @Column()
  finalDatetime: Date;

  @ManyToOne(() => User, (user) => user.events)
  user: User;

  @ManyToMany(() => User)
  @JoinTable()
  guests: User[];
}
