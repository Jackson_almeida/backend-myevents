import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Event } from './event.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 30 })
  username: string;

  @Column({ length: 40, unique: true })
  email: string;

  @Column({ length: 255 })
  password: string;

  @OneToMany(() => Event, (event) => event.user)
  events: Event[];
}
