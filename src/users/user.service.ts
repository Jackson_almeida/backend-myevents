import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { CreateUserDto } from '../dto/create-user.dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async getAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  async getById(id: number): Promise<User> {
    return this.userRepository.findOne(id);
  }

  async getByEmail(email: string): Promise<User> {
    return this.userRepository.findOne(email);
  }

  // Verificar email
  async getByUserEmail(email: string): Promise<User | undefined> {
    return this.userRepository.findOne({ email: email });
  }

  async createUser(createUserDto: CreateUserDto): Promise<User> {
    const user = new User();
    user.username = createUserDto.username;
    user.email = createUserDto.email;
    user.password = bcrypt.hashSync(createUserDto.password, 8);
    return this.userRepository.save(user);
  }

  async updateUser(id: number, createUserDto: CreateUserDto): Promise<User> {
    const user = await this.userRepository.findOne(id);
    user.username = createUserDto.username;
    user.email = createUserDto.email;
    user.password = createUserDto.password;
    return this.userRepository.save(user);
  }

  async deleteUserId(id: number): Promise<string> {
    await this.userRepository.delete(id);
    return 'Usuário deletado com sucesso!';
  }
}
